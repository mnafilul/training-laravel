<?php

namespace App\Http\Controllers;
use App\Proyek;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyek = Proyek::all();
        return view('home',compact('proyek'));
    }
    public function store(Request $request)
{
    $proyek = new Proyek();
    $proyek->nama_proyek = $request->get('nama_proyek');
    $proyek->biaaya = $request->get('biaaya');
    $proyek->save();
 
    return redirect('proyek')->with('success','Data Proyek has been added');
}
    public function create()
    {
        $proyek = Proyek::all();
        return view('create',compact('proyek'));
    }
    public function edit($id)
    {
        $proyek = Proyek::find($id);
        return view('edit',compact('proyek','id'));
    }

    public function update(Request $request, $id)
    {
      $proyek= Proyek::find($id);
      $proyek->nama_proyek=$request->get('nama_proyek');
      $proyek->biaaya=$request->get('biaaya');
      $proyek->save();
     return redirect('proyek')->with('success','Data Proyek has been updated');
    }
    public function destroy($id)
    {
        $proyek = Proyek::find($id);
        $proyek->delete();
        return redirect('proyek')->with('success','Data Proyek Has Been Deleted');
    }
}
