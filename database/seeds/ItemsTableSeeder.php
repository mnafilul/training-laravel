<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $faker      = Faker::create('App/Item');

        DB::table('proyeks')->insert([
            'id'       			=> $faker->numberBetween(1,100),
            'nama_proyek'       => $faker->sentence(),
            'biaaya'   			=> $faker->numberBetween(1,100),
            'created_at'    	=> Carbon::now(),
            'updated_at'    	=> Carbon::now()
        ]);
    }
}
