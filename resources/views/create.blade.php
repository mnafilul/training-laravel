<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>CRUD</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
  </head>
  <body>
    <div class="container">
      <h2>Create Proyek</h2><br/>
      <form method="post" action="{{route('createdata.home')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-md-12"></div>
          <div class="form-group col-md-4">
            <label for="nama_proyek">Nama Proyek</label>
            <input type="text" class="form-control" id="nama_proyek" name="nama_proyek">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12"></div>
            <div class="form-group col-md-4">
            <label for="biaaya">Biaya</label>
            <input type="text" class="form-control" id="biaaya" name="biaaya">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12"></div>
          <div class="form-group col-md-4" style="margin-top:10px">
            <button type="submit" class="btn btn-success" name="submit">Submit</button>
            <button type="submit" onclick="history.go(-1);" class="btn btn-warning">Back</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>