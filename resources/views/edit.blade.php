<!-- edit.blade.php -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Laravel CRUD - EDIT </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Update Proyek</h2><br />
        <form method="POST" action="{{route('update.home',['id' => $proyek->id])}}" enctype="multipart/form-data">
        {{ csrf_field() }}
       
        <div class="row">
          <div class="col-md-12"></div>
          <div class="form-group col-md-4">
            <label for="nama">Nama Proyek:</label>
            <input type="text" class="form-control" name="nama_proyek" value="{{$proyek->nama_proyek}}">
          </div>
        </div>
         <div class="row">
          <div class="col-md-12"></div>
          <div class="form-group col-md-4">
            <label for="biaaya">Biaya Proyek:</label>
            <input type="text" class="form-control" name="biaaya" value="{{$proyek->biaaya}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12"></div>
          <div class="form-group col-md-12" style="margin-top:10px">
            <button type="submit" class="btn btn-success">Update</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>