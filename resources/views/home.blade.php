<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>List Proyek</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
        <h1>List Data Proyek</h1>  
        <a href="{{route('create.home')}}" class="btn btn-success" id="tambah_data">Tambah Data</a>
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Biaya</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
 
      @foreach($proyek as $proyek)
      <tr>
        <td>{{$proyek['id']}}</td>
        <td>{{$proyek['nama_proyek']}}</td>
        <td>{{$proyek['biaaya']}}</td>
        <td>
            <a href="{{route('edit.home', ['id' => $proyek->id])}}" class="btn btn-warning">Edit</a>
            <a href="{{route('destroy.home', ['id' => $proyek->id])}}">
              <button class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button></a>
                {{ csrf_field() }}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
  <script>
      $(document).on('click', '.button', function (e) {
          e.preventDefault();
          var id = $(this).data('id');
          swal({
                  title: "Are you sure!",
                  type: "error",
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes!",
                  showCancelButton: true,
              },
              function() {
                  $.ajax({
                      type: "POST",
                      url: "{{url('room/delete')}}",
                      data: {id:id},
                      success: function (data) {

                          }
                  });
          });
      });
</script>
</html>