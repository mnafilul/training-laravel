<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
route::resource('proyek','HomeController');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/create', 'HomeController@create')->name('create.home');
Route::get('/edit/{id}', 'HomeController@edit')->name('edit.home');
Route::post('/update/{id}', 'HomeController@update')->name('update.home');
Route::get('/delete/{id}', 'HomeController@destroy')->name('destroy.home');
Route::post('/create', 'HomeController@store')->name('createdata.home');
