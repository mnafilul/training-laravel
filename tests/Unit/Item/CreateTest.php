<?php
namespace Item;

use Faker\Factory;
use Carbon\Carbon;

class CreateTest extends \Codeception\Test\Unit
{
  public function testSomeFeature()
    {
        $faker          = Factory::create('App\Item');
        $id             = $faker->numberBetween(1,100);
        $nama_proyek    = $faker->sentence();
        $biaaya         = $faker->randomNumber();
        $created_at     = Carbon::now();
        $updated_at     = Carbon::now();

        $this->tester->haveRecord(
            'App\Proyek',
            ['id' => $id, 'nama_proyek' => $nama_proyek, 'biaaya' => $biaaya, 'created_at' => $created_at, 'updated_at' => $updated_at]
        );

        $this->tester->seeRecord(
            'proyeks',
            ['id' => $id, 'nama_proyek' => $nama_proyek, 'biaaya' => $biaaya, 'created_at' => $created_at, 'updated_at' => $updated_at]
        );
    }
}